from browser import document, html
import csv
document <= html.H1("Questionnaire du Choixpeau Magique : ")
document <= html.H2("Voici quleques questions: ")

with open("Questionnaire_profil.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    questionnaire_tab = [{key : value for key, value in element.items()} for element in reader]

print(questionnaire_tab)
reponses = []
numero_questions = 0

def click_reponses(event):
    global reponses
    global numero_questions
    print(event.target.value)
    reponses.append(eval(questionnaire_tab[numero_questions]['Pondération' + str(event.target.value)]))
    print(reponses)

    
    document["question"].textContent = questionnaire_tab[numero_questions]['Question']
    for i in range(1, 4):
        document['reponse' + str(i)].textContent = questionnaire_tab[numero_questions]['Réponse' + str(i)]
    numero_questions += 1 

def distance (perso1, perso2):
    return sqrt((perso1['Courage'] - int(perso2['Courage'])) ** 2
              + (perso1['Ambition'] - int(perso2['Ambition'])) ** 2
              + (perso1['Intelligence'] - int(perso2['Intelligence'])) ** 2 
              + (perso1['Good'] - int(perso2['Good'])) ** 2)

document <= html.H3('LA QUESTION', id="question")
for i in range(1, 4):
    document <= html.P(html.BUTTON('reponse' + str(i), id = 'reponse' + str(i), value = i ))
    document["reponse" + str(i)].bind("click", click_reponses)



