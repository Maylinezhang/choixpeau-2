from browser import document, html
import csv
from math import sqrt

document <= html.H1("Questionnaire du Choixpeau Magique : ")
document <= html.H2("Voici quelques questions: ")

#importation css
with open("Questionnaire_profil.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    questionnaire_tab = [{key : value for key, value in element.items()} for element in reader]
print(questionnaire_tab)

#création du nouveau profil sous forme de tuple puis calcul des moyennes
reponses = []
numero_questions = 0
profil = (0,0,0,0)

def click_reponses(event):
    global reponses
    global numero_questions
    global profil
    print(event.target.value)
    reponses.append(eval(questionnaire_tab[numero_questions]['Pondération' + str(event.target.value)]))
    for tuple in reponses:
        if numero_questions == 10:
            profil[0] += tuple [0]
            profil[1] += tuple [1]
            profil[2] += tuple [2]
            profil[3] += tuple [3]
        profil = ("courage" : profil[0]/10, "intelligence" : profil[1]/10, "ambition" : profil[2]/10, "good" : profil[3]/10)
        else:
            document["question"].textContent = questionnaire_tab[numero_questions]['Question']
            for i in range(1, 4):
            document['reponse' + str(i)].textContent = questionnaire_tab[numero_questions]['Réponse' + str(i)]
            numero_questions += 1
            document <= html.H3('Vous pouvez, grâce à ce petit questionnaire découvrir votre maison chez le mystérieux école des sorciers et les cinq personnages qui vous ressemblent le plus ', id="question")
            document <= html.P(html.BUTTON('reponse' + str(i), id = 'reponse' + str(i), value = i ))
            document["reponse" + str(i)].bind("click", click_reponses)

resultat = 'none'
def resultat(event):
    global resultat
    resultat.append(houses_list_sorted)
    document["resultat"].bind("click", resultat)
    document <= html.P(html.BUTTON("Voici votre maison", id = 'resultat'))

K = 5

#Fonctions et importation de la Partie 1
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value for key, value in element.items()} for element in reader]

poudlard_characters = []

for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)


poudlard_characters = [] 

for poudlard_character in questionnaire_tab:
    poudlard_characters.append(poudlard_character)

def distance (perso1, perso2):
    return sqrt((perso1['Courage'] - int(perso2['Courage'])) ** 2
              + (perso1['Ambition'] - int(perso2['Ambition'])) ** 2
              + (perso1['Intelligence'] - int(perso2['Intelligence'])) ** 2 
              + (perso1['Good'] - int(perso2['Good'])) ** 2)

def add_distances(table_poudlard, perso_inconnu):
    for perso in table_poudlard:
        perso['Distance'] = distance(perso_inconnu, perso)
    return table_poudlard

for profil in questionnaire_tab:

    distance_list = add_distances(poudlard_characters, profil)
    voisins = sorted(distance_list, key=lambda x: x['Distance'])
    k_voisins = voisins[:K]

    print('Les {K} plus proches voisins de vous sont :')
    houses_count = {'Gryffindor': 0, 'Hufflepuff':0, 'Ravenclaw' :0, 'Slytherin' :0 }
    for voisin in k_voisins:
        
        print( voisin['Name'], voisin['House'])

        if voisin['House'] == 'Gryffindor':
            houses_count['Gryffindor'] += 1 
        elif voisin['House'] == 'Hufflepuff':
            houses_count['Hufflepuff'] += 1
        elif voisin['House'] == 'Ravenclaw':
            houses_count['Ravenclaw'] += 1
        elif voisin['House'] == 'Slytherin':
            houses_count['Slytherin'] += 1

        houses_list = list(houses_count.items())
        houses_list_sorted = sorted(houses_list, key=lambda x: x[1])

        print(f'Votre maison est {houses_list_sorted[-1][0]} !')


